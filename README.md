# C(ache)ThruL(east)R(ecently)U(sed)
A data structure for retaining a cache of items, with logic to track which
is the least recently retrieved. 
- "thru" module: These let you define a closure for the logic 'under' the cache.
- (To do?) "thru_async" module: These are "thru", but the closure can be async.
- "storage" module: If a closure doesn't work for your needs, you can push
  things into the cache more directly (coming soon).
