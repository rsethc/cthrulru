use {
    super::lru_storage::*,
    rand::{thread_rng, Rng},
};

#[test]
fn place_evict_rinse_repeat() {
    let mut lru = LRUStorage::default();

    for _tries in 0..10 {
        for i in 1..=10 {
            assert!(lru.get(&i).is_none());

            lru.place(i, i * 10);
            let got = *lru.get(&i).unwrap();
            assert_eq!(got, i * 10);
        }

        for i in 1..=10 {
            let (evicted_key, evicted_item) = lru.evict().unwrap();
            assert_eq!(evicted_key, i);
            assert_eq!(evicted_item, i * 10);

            assert!(lru.get(&i).is_none());
        }
    }
}

#[test]
fn reinsert_existing_key() {
    let mut lru = LRUStorage::with_capacity(10);

    for _tries in 0..10 {
        for i in 1..=10 {
            assert!(lru.get(&i).is_none());

            lru.place(i, i * 10);
            let got = *lru.get(&i).unwrap();
            assert_eq!(got, i * 10);

            lru.place(i, i * 40);
            let got = *lru.get(&i).unwrap();
            assert_eq!(got, i * 40);
        }

        for i in 1..=10 {
            let (evicted_key, evicted_item) = lru.evict().unwrap();
            assert_eq!(evicted_key, i);
            assert_eq!(evicted_item, i * 40);

            assert!(lru.get(&i).is_none());
        }
    }
}

#[test]
fn verify_freshened_on_access() {
    let mut lru = LRUStorage::with_capacity(10);

    for _tries in 0..10 {
        for i in 1..=10 {
            assert!(lru.get(&i).is_none());

            lru.place(i, i * 10);
            let got = *lru.get(&i).unwrap();
            assert_eq!(got, i * 10);
        }

        println!("after adding all 10 -> {:#?}", lru);

        for i in (1..=10).rev() {
            println!("before revisiting {} -> {:#?}", i, lru);
            let got = *lru.get(&i).unwrap();
            assert_eq!(got, i * 10);
        }

        println!("after revisiting all 10 backwards -> {:#?}", lru);

        for i in (1..=10).rev() {
            println!("before eviction {} -> {:#?}", i, lru);
            let (evicted_key, evicted_item) = lru.evict().unwrap();
            assert_eq!(evicted_key, i);
            assert_eq!(evicted_item, i * 10);

            assert!(lru.get(&i).is_none());
        }
    }
}

fn randovec<T>(items: &mut Vec<T>) {
    let len = items.len();
    for i in 0..len {
        items.swap(i, thread_rng().gen_range(0..len));
    }
}

#[test]
fn consistent_rando() {
    let mut lru = LRUStorage::with_capacity(10);

    for _tries in 0..10 {
        let mut keys = (1..=10).collect();
        randovec(&mut keys);
        for i in keys {
            assert!(lru.get(&i).is_none());

            lru.place(i, i * 10);
            let got = *lru.get(&i).unwrap();
            assert_eq!(got, i * 10);
        }

        println!("after adding all 10 -> {:#?}", lru);

        for _rando_iter in 1..=100 {
            let i = rand::thread_rng().gen_range(1..=10);

            let got = *lru.get(&i).unwrap();
            assert_eq!(got, i * 10);
        }

        let mut seen = [false; 10];
        while let Some((key, item)) = lru.evict() {
            assert!(!seen[key - 1]);
            assert_eq!(item, key * 10);
            seen[key - 1] = true;
        }
        for seen in seen {
            assert!(seen);
        }

        for i in 1..=10 {
            assert!(lru.get(&i).is_none());
        }
    }
}
