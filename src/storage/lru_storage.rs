use std::{collections::HashMap, fmt::Debug, hash::Hash, mem};

enum Occupancy<OccupyingType> {
    Occupied(OccupyingType),
    Unoccupied(Option<usize>), // Points to next Unoccupied in the containing Vec.
}
impl<OccupyingType> Occupancy<OccupyingType> {
    fn occupied(&mut self) -> Option<&mut OccupyingType> {
        match self {
            Self::Occupied(ref mut occupied) => Some(occupied),
            Self::Unoccupied(_) => None,
        }
    }
    fn evict(&mut self, next_unoccupied: Option<usize>) -> OccupyingType {
        match mem::replace(self, Self::Unoccupied(next_unoccupied)) {
            Self::Unoccupied(_) => panic!("LRUStorage eviction of unoccupied slot"),
            Self::Occupied(item) => item,
        }
    }
    fn place(&mut self, item: OccupyingType) -> Option<usize> {
        match mem::replace(self, Self::Occupied(item)) {
            Self::Occupied(_) => panic!("LRUStorage placement into occupied slot"),
            Self::Unoccupied(next_unoccupied) => next_unoccupied,
        }
    }
}

struct CacheEntry<KeyType, ItemType> {
    older_use: Option<usize>,
    newer_use: Option<usize>,
    key: KeyType,
    item: ItemType,
}

pub struct LRUStorage<KeyType, ItemType> {
    storage: Vec<Occupancy<CacheEntry<KeyType, ItemType>>>,
    lookup: HashMap<KeyType, usize>,
    oldest_use: Option<usize>,
    newest_use: Option<usize>,
    next_unoccupied: Option<usize>,
}

impl<KeyType, ItemType> Default for LRUStorage<KeyType, ItemType> {
    fn default() -> Self {
        Self {
            storage: Vec::new(),
            lookup: HashMap::new(),
            oldest_use: None,
            newest_use: None,
            next_unoccupied: None,
        }
    }
}

impl<KeyType: Clone + Eq + Hash, ItemType: Clone> LRUStorage<KeyType, ItemType> {
    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            storage: Vec::with_capacity(capacity),
            lookup: HashMap::with_capacity(capacity),
            oldest_use: None,
            newest_use: None,
            next_unoccupied: None,
        }
    }

    // Free up space in the in-memory cache, sending the entry to underlying write if dirty.
    pub fn evict(&mut self) -> Option<(KeyType, ItemType)> {
        if let Some(evicting_id) = self.oldest_use {
            let evicting_slot = &mut self.storage[evicting_id];
            let evicting_entry = evicting_slot.occupied().unwrap();
            self.lookup.remove(&evicting_entry.key);
            let evicted = evicting_slot.evict(self.next_unoccupied);
            self.next_unoccupied = Some(evicting_id);
            self.oldest_use = evicted.newer_use;
            if let Some(new_oldest) = evicted.newer_use {
                self.storage[new_oldest].occupied().unwrap().older_use = None;
            } else {
                self.newest_use = None;
            }
            let (key, item) = (evicted.key, evicted.item);
            Some((key, item))
        } else {
            None
        }
    }

    fn freshen(&mut self, freshening_id: usize) {
        if let Some(newer_id) = self.storage[freshening_id].occupied().unwrap().newer_use {
            let older_id = self.storage[freshening_id].occupied().unwrap().older_use;
            self.storage[newer_id].occupied().unwrap().older_use = older_id;
            if let Some(older_id) = older_id {
                self.storage[older_id].occupied().unwrap().newer_use = Some(newer_id);
            } else {
                self.oldest_use = Some(newer_id);
            }
            let freshening_entry = self.storage[freshening_id].occupied().unwrap();
            freshening_entry.older_use = self.newest_use;
            freshening_entry.newer_use = None;
            if let Some(prior_newest) = self.newest_use {
                self.storage[prior_newest].occupied().unwrap().newer_use = Some(freshening_id);
            }
            self.newest_use = Some(freshening_id);
        }
        // If there is no newer, we don't need to do anything.
    }

    // Return cached copy, otherwise pull from underlying as not dirty.
    pub fn get(&mut self, key: &KeyType) -> Option<&ItemType> {
        if let Some(&location) = self.lookup.get(key) {
            // Already have it in cache, cool.
            // Mark it as most recently used.
            self.freshen(location);
            // This is an unwrap because it shouldn't have
            // been in lookup table if it was unoccupied.
            Some(&self.storage[location].occupied().unwrap().item)
        } else {
            None
        }
    }

    // Return cached copy, otherwise pull from underlying as not dirty.
    pub fn place(&mut self, key: KeyType, item: ItemType) {
        if let Some(&location) = self.lookup.get(&key) {
            // Already have it in cache, replace the value and
            // then mark it as most recently used.
            self.storage[location].occupied().unwrap().item = item;
            self.freshen(location);
        } else {
            // We don't have anything of this key, make a new
            // entry to store it.
            let new_entry = CacheEntry {
                key: key.clone(),
                item,
                newer_use: None,
                older_use: self.newest_use,
            };

            let location = if let Some(next_unoccupied) = self.next_unoccupied {
                // Reuse this slot which is unoccupied.
                let reusing_slot = &mut self.storage[next_unoccupied];
                self.next_unoccupied = reusing_slot.place(new_entry);
                next_unoccupied
            } else {
                // All slots occupied, expand storage with a new slot.
                let location = self.storage.len();
                self.storage.push(Occupancy::Occupied(new_entry));
                location
            };

            if let Some(prior_newest) = self.newest_use {
                self.storage[prior_newest].occupied().unwrap().newer_use = Some(location);
            } else {
                self.oldest_use = Some(location);
            }
            self.newest_use = Some(location);
            self.lookup.insert(key, location);
        }
    }
}

impl<KeyType: Debug, ItemType: Debug> Debug for CacheEntry<KeyType, ItemType> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("CacheEntry")
            .field("older_use", &self.older_use)
            .field("newer_use", &self.newer_use)
            .field("key", &self.key)
            .field("item", &self.item)
            .finish()
    }
}

impl<KeyType: Debug, ItemType: Debug> Debug for LRUStorage<KeyType, ItemType> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("LRUStorage")
            .field("storage", &self.storage)
            .field("lookup", &self.lookup)
            .field("oldest_use", &self.oldest_use)
            .field("newest_use", &self.newest_use)
            .field("next_unoccupied", &self.next_unoccupied)
            .finish()
    }
}

impl<ItemType: Debug> Debug for Occupancy<ItemType> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Occupied(arg0) => f.debug_tuple("Occupied").field(arg0).finish(),
            Self::Unoccupied(arg0) => f.debug_tuple("Unoccupied").field(arg0).finish(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn place_evict_rinse_repeat() {
        for _tries in 0..10 {
            let mut lru = LRUStorage::default();

            for i in 1..=10 {
                assert!(lru.get(&i).is_none());

                lru.place(i, i * 10);
                let got = *lru.get(&i).unwrap();
                assert_eq!(got, i * 10);
            }

            for i in 1..=10 {
                let (evicted_key, evicted_item) = lru.evict().unwrap();
                assert_eq!(evicted_key, i);
                assert_eq!(evicted_item, i * 10);

                assert!(lru.get(&i).is_none());
            }
        }
    }

    #[test]
    fn reinsert_existing_key() {
        for _tries in 0..10 {
            let mut lru = LRUStorage::with_capacity(10);

            for i in 1..=10 {
                assert!(lru.get(&i).is_none());

                lru.place(i, i * 10);
                let got = *lru.get(&i).unwrap();
                assert_eq!(got, i * 10);

                lru.place(i, i * 40);
                let got = *lru.get(&i).unwrap();
                assert_eq!(got, i * 40);
            }

            for i in 1..=10 {
                let (evicted_key, evicted_item) = lru.evict().unwrap();
                assert_eq!(evicted_key, i);
                assert_eq!(evicted_item, i * 40);

                assert!(lru.get(&i).is_none());
            }
        }
    }

    #[test]
    fn verify_freshened_on_access() {
        for _tries in 0..10 {
            let mut lru = LRUStorage::with_capacity(10);

            for i in 1..=10 {
                assert!(lru.get(&i).is_none());

                lru.place(i, i * 10);
                let got = *lru.get(&i).unwrap();
                assert_eq!(got, i * 10);
            }

            println!("after adding all 10 -> {:#?}", lru);

            for i in (1..=10).rev() {
                println!("before revisiting {} -> {:#?}", i, lru);
                let got = *lru.get(&i).unwrap();
                assert_eq!(got, i * 10);
            }

            println!("after revisiting all 10 backwards -> {:#?}", lru);

            for i in (1..=10).rev() {
                println!("before eviction {} -> {:#?}", i, lru);
                let (evicted_key, evicted_item) = lru.evict().unwrap();
                assert_eq!(evicted_key, i);
                assert_eq!(evicted_item, i * 10);

                assert!(lru.get(&i).is_none());
            }
        }
    }

    #[test]
    fn consistent_rando() {}
}
