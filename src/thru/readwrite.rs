use std::{collections::HashMap, hash::Hash};

struct CacheEntry<KeyType, ItemType> {
    older_use: Option<usize>,
    newer_use: Option<usize>,
    key: KeyType,
    item: ItemType,
    dirty: bool,
}

pub struct RwThruLRU<'a, KeyType: Clone + Eq + Hash, ItemType: Clone> {
    storage: Vec<CacheEntry<KeyType, ItemType>>,
    lookup: HashMap<KeyType, usize>,
    oldest_use: Option<usize>,
    newest_use: Option<usize>,
    under: Box<dyn ReadWrite<KeyType, ItemType> + 'a>,
}

pub trait ReadWrite<KeyType, ItemType> {
    fn read(&mut self, key: &KeyType) -> ItemType;
    fn write(&mut self, key: &KeyType, item: ItemType);
}

impl<'a, KeyType: Clone + Eq + Hash, ItemType: Clone> RwThruLRU<'a, KeyType, ItemType> {
    pub fn new(capacity: usize, under: impl ReadWrite<KeyType, ItemType> + 'a) -> Self {
        Self {
            storage: Vec::with_capacity(capacity),
            lookup: HashMap::with_capacity(capacity),
            oldest_use: None,
            newest_use: None,
            under: Box::new(under),
        }
    }

    // Free up space in the in-memory cache, sending the entry to underlying write if dirty.
    fn evict(&mut self) -> usize {
        if let Some(oldest) = self.oldest_use {
            let newer = self.storage[oldest].newer_use;
            self.oldest_use = newer;
            if let Some(newer) = newer {
                self.storage[newer].older_use = None;
            } else {
                // Weird but allowed edge case (cache capacity of 1)
                self.newest_use = None;
            }
            let removing = &self.storage[oldest];
            self.under.write(&removing.key, removing.item.clone());
            self.lookup.remove(&removing.key);
            oldest
        } else {
            // This function should never be called without existing items
            unreachable!()
        }
    }

    fn freshen(&mut self, location: usize) {
        if let Some(newer) = self.storage[location].newer_use {
            let older = self.storage[location].older_use;
            self.storage[newer].older_use = older;
            if let Some(older) = older {
                self.storage[older].newer_use = Some(newer);
            } else {
                self.oldest_use = Some(newer);
            }
            self.storage[location].older_use = self.newest_use;
            self.newest_use = Some(location);
        }
        // If there is no newer, we don't need to do anything.
    }

    // Return cached copy, otherwise pull from underlying as not dirty.
    pub fn get(&mut self, key: KeyType) -> ItemType {
        if let Some(&location) = self.lookup.get(&key) {
            // Already have it in cache, cool.
            // Mark it as most recently used.
            self.freshen(location);
            self.storage[location].item.clone()
        } else {
            // We'll need to take up a cache entry with something new.
            // Evict the least recently used item to make room if the cache is saturated.

            let loaded = self.under.read(&key);
            let new_entry = CacheEntry {
                key: key.clone(),
                item: loaded.clone(),
                dirty: false,
                newer_use: None,
                older_use: self.newest_use,
            };

            let location = if self.storage.len() >= self.storage.capacity() {
                let freed = self.evict();
                self.storage[freed] = new_entry;
                freed
            } else {
                let added = self.storage.len();
                self.storage.push(new_entry);
                added
            };

            if let Some(prior_newest) = self.newest_use {
                self.storage[prior_newest].newer_use = Some(location);
            } else {
                self.oldest_use = Some(location);
            }
            self.newest_use = Some(location);
            self.lookup.insert(key, location);
            loaded
        }
    }

    // Update or insert this into cache, with dirty flag set.
    pub fn set(&mut self, key: KeyType, item: ItemType) {
        let location = if let Some(existing) = self.lookup.get(&key) {
            *existing
        } else {
            self.evict()
        };
        self.storage[location].item = item;
        self.storage[location].dirty = true;
    }

    // Flush any 'dirty' cache entries to underlying write.
    pub fn flush(&mut self) {
        for entry in &mut self.storage {
            if entry.dirty {
                entry.dirty = false;
                self.under.write(&entry.key, entry.item.clone());
            }
        }
    }
}

impl<KeyType: Clone + Eq + Hash, ItemType: Clone> Drop for RwThruLRU<'_, KeyType, ItemType> {
    fn drop(&mut self) {
        self.flush();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    struct VecRW<'a, ItemType> {
        vec: &'a mut Vec<ItemType>,
    }
    impl<ItemType: Clone> ReadWrite<usize, ItemType> for VecRW<'_, ItemType> {
        fn read(&mut self, key: &usize) -> ItemType {
            println!("getting {}", key);
            self.vec[*key].clone()
        }
        fn write(&mut self, key: &usize, item: ItemType) {
            println!("evicted {}", key);
            self.vec[*key] = item;
        }
    }

    #[test]
    fn asdf() {
        let mut underlying = vec![0, 10, 20, 30, 40, 50, 60, 70, 80, 90];
        {
            let rw = VecRW {
                vec: &mut underlying,
            };

            let mut lru = RwThruLRU::new(5, rw);
            for i in 0..=9 {
                println!("{} -> {}", i, lru.get(i));
            }
            for i in (0..=9).rev() {
                println!("{} -> {}", i, lru.get(i));
            }
            for i in 0..=9 {
                let n = lru.get(i);
                lru.set(i, n + 1);
            }
            for i in 0..=9 {
                let n = lru.get(i);
                lru.set(i, n + 1);
            }
            for i in 0..=9 {
                let n = lru.get(i);
                assert_eq!(n, i * 10 + 2);
            }
            //lru.flush();
        }
        for i in 0..=9 {
            let n = underlying[i];
            assert_eq!(n, i * 10 + 2);
        }
    }
}
