use std::{collections::HashMap, hash::Hash};

struct CacheEntry<KeyType, ItemType> {
    older_use: Option<usize>,
    newer_use: Option<usize>,
    key: KeyType,
    item: ItemType,
}

pub struct ReadThruLRU<'a, KeyType, ItemType> {
    storage: Vec<CacheEntry<KeyType, ItemType>>,
    lookup: HashMap<KeyType, usize>,
    oldest_use: Option<usize>,
    newest_use: Option<usize>,
    under_read: Box<dyn Fn(&KeyType) -> ItemType + 'a>,
}

impl<'a, KeyType: Clone + Eq + Hash, ItemType: Clone> ReadThruLRU<'a, KeyType, ItemType> {
    pub fn new(capacity: usize, under_read: impl Fn(&KeyType) -> ItemType + 'a) -> Self {
        Self {
            storage: Vec::with_capacity(capacity),
            lookup: HashMap::with_capacity(capacity),
            oldest_use: None,
            newest_use: None,
            under_read: Box::new(under_read),
        }
    }

    // Free up space in the in-memory cache, sending the entry to underlying write if dirty.
    fn evict(&mut self) -> usize {
        if let Some(oldest) = self.oldest_use {
            let newer = self.storage[oldest].newer_use;
            self.oldest_use = newer;
            if let Some(newer) = newer {
                self.storage[newer].older_use = None;
            } else {
                // Weird but allowed edge case (cache capacity of 1)
                self.newest_use = None;
            }
            self.lookup.remove(&self.storage[oldest].key);
            oldest
        } else {
            // This function should never be called without existing items
            unreachable!()
        }
    }

    fn freshen(&mut self, location: usize) {
        if let Some(newer) = self.storage[location].newer_use {
            let older = self.storage[location].older_use;
            self.storage[newer].older_use = older;
            if let Some(older) = older {
                self.storage[older].newer_use = Some(newer);
            } else {
                self.oldest_use = Some(newer);
            }
            self.storage[location].older_use = self.newest_use;
            self.newest_use = Some(location);
        }
        // If there is no newer, we don't need to do anything.
    }

    // Return cached copy, otherwise pull from underlying as not dirty.
    pub fn get(&mut self, key: KeyType) -> ItemType {
        if let Some(&location) = self.lookup.get(&key) {
            // Already have it in cache, cool.
            // Mark it as most recently used.
            self.freshen(location);
            self.storage[location].item.clone()
        } else {
            // We'll need to take up a cache entry with something new.
            // Evict the least recently used item to make room if the cache is saturated.

            let loaded = (self.under_read)(&key);
            let new_entry = CacheEntry {
                key: key.clone(),
                item: loaded.clone(),
                newer_use: None,
                older_use: self.newest_use,
            };

            let location = if self.storage.len() >= self.storage.capacity() {
                let freed = self.evict();
                self.storage[freed] = new_entry;
                freed
            } else {
                let added = self.storage.len();
                self.storage.push(new_entry);
                added
            };

            if let Some(prior_newest) = self.newest_use {
                self.storage[prior_newest].newer_use = Some(location);
            } else {
                self.oldest_use = Some(location);
            }
            self.newest_use = Some(location);
            self.lookup.insert(key, location);
            loaded
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn gen_cache() {
        let mut lru = ReadThruLRU::new(5, |key: &i32| {
            println!("generating for {}", key);
            key * key
        });
        for i in 1..=10 {
            println!("{} -> {}", i, lru.get(i));
        }
        for i in 6..=10 {
            println!("{} -> {}", i, lru.get(i));
        }
    }
    #[test]
    fn vec_cache() {
        let mut source = vec![0, 10, 20, 30, 40, 50, 60, 70, 80, 90];
        {
            let mut lru = ReadThruLRU::new(5, |key: &usize| {
                println!("get {}", key);
                source[*key]
            });
            for i in 0..=9 {
                println!("{} -> {}", i, lru.get(i));
            }
            for i in 5..=9 {
                println!("{} -> {}", i, lru.get(i));
            }
        }
        source.push(100);
    }
}
